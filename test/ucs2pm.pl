#!/usr/bin/perl
######################################################################
#
# 한글 음절문자를 초성/중성/종성으로 분리하여 로마자 표기법 전자법으로 출력
# 
######################################################################
## strict 사용하지 않음
#use strict;
#use warnings;

use utf8;
use open ':std', ':utf8';

foreach (<>) {
  utf8::upgrade($_);
  LF2LFCR($_);
  print hangul2jaso($_);
}

sub hangul2jaso { # 유니코드 한글 문자열을 입력 받음
  my ($a, $b, $c); # 자소 버퍼: 초성/중성/종성 순

# 초성
  my @ChoSung   = ("g", "gg", "n", "d", "dd", "l", "m", "b", "bb", "s", "ss", "=", "j", "jj", "c", "k", "t", "p", "h");

# 중성
  my @JwungSung = ("a", "ay", "qa", "qay", "e", "ey", "qe", "qey", "o", "wa", "way", "oy", "qo", "u", "we", "wey", "wi", "qu", "v", "vy", "i");

# 종성
  my @JongSung  = (0, "g", "gg", "gs", "n", "nj", "nh", "d", "l", "lg", "lm", "lb", "ls", "lt", "lp", "lh", "m", "b", "bs", "s", "ss", "0", "j", "c", "k", "t", "p", "h");

  my @input_chars = unpack("U*", $_[0]); # 한글 유니코드 문자열을 16진수 배열로
  my $result;

  foreach (@input_chars) {
     # "AC00:가" ~ "D7A3:힣" 에 속한 글자면 분해.
     if ($_ >= 0xAC00 && $_ <= 0xD7A3) {

        $c = $_ - 0xAC00;
        $a = $c / (21 * 28);
        $c = $c % (21 * 28);
        $b = $c / 28;
        $c = $c % 28;

        $a = int($a); $b = int($b); $c = int($c);
        $result .= $ChoSung[$a] . $JwungSung[$b];
        $result .= $JongSung[$c] if ($c); # $c가 0이 아닐 때.
     } else {
        $result .= chr($_);
     }
  }
  $result;
}


sub LF2LFCR {
  $_[0] =~ s/\n/\r\n/g if ($_[0] !~ /\r\n/); # 도스 텍스트가 아니면
}

