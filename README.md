# Oesolscript LaTeX package

## About

`oesolscript` is a LaTeX package which allows to typeset `pureosseugi` (_disassembled_) Hangul scripting system suggested by [Choe Hyeon-bae](https://en.wikipedia.org/wiki/Choe_Hyeon-bae) (1894--1970) whose art name was *Oesol*. He not only suggested the `pureosseugi` system but also designed a newly shaped Hangul alphabets. And the `Oesol` font developed by Tzetachi is the implementation of them. This package uses and requires `Oesol` font, which can be downloaded from [github.com/Tzetachi](https://github.com/Tzetachi/Computer-Modern-Unicode-Oesol).

pdfLaTeX is not supported. XeLaTeX or LuaLaTeX is required.

## Version and License

* version 0.3.5: 2020/06/01
* Copyright (C) 2020 Nova de Hi
* MIT license

## Usage

```
\usepackage[fontfile=<file>]{oesolscript}
```

## Command and Environment

* command `\oesol`.
* environment `{oesoltext}`.
* All Korean letters should be input according to the _Romanization of Korean_ transliteration rules.

## Example

```latex
\oesol{Uri neun ije} \emph{\oesol{Joseon}} \oesol{i
Dog-libgug im gwa} \emph{\oesol{Joseon salam}}
\oesol{i Jajumin im eul seoneonhanda.}\\
\oesol{A, sincheonji ga anjeon e jeon\-gae doedoda.}
```

## ![screenshot](screenshot.png)

## Thanks

At first, this package was discussed, developed and distributed on [KTUG](http://www.ktug.org/xe/index.php?mid=KTUG_open_board&document_srl=235641). I thank Tzetachi, the author of the font, and other commenters.

